/*
Original work Copyright (C) 2013 Olivier Goffart <ogoffart@woboq.com>
http://woboq.com/blog/property-bindings-in-cpp.html
Modified work Copyright (c) 2014 James Brown

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once

#include <I_Property.h>
#include <PropertyEvaluationContext.h>
#include <functional>

template <typename T>
class Property : public I_Property
{
   using Func = std::function<T()>;
public:
   Property(const T& value);
   Property(Func binding);
   Property(Property<T>&& other);

   Property<T>& operator=(const T& rhs);
   Property<T>& operator=(const Func& rhs);
   operator const T&() const;

protected:
   virtual void clearDependent();
   virtual void evaluate();

protected:
   T value_;
   Func binding_;

private:
   Q_DISABLE_COPY(Property)
};

template <typename T> Property<T>::Property(const T& value)
   : value_(value)
{
}

template <typename T> Property<T>::Property(Func binding)
   : binding_(binding)
{
   evaluate();
}

template <typename T> Property<T>::Property(Property<T>&& other)
{
   value_ = other.value_;
   binding_ = other.binding_;
   evaluate();
}

template <typename T> Property<T>& Property<T>::operator=(const T& rhs)
{
   emit reset();
   binding_ = Func();
   value_ = rhs;
   emit changed();
   return *this;
}

template <typename T> Property<T>& Property<T>::operator=(const Func& rhs)
{
   emit reset();
   binding_ = rhs;
   evaluate();
   return *this;
}

template <typename T> Property<T>::operator const T&() const
{
   PropertyEvaluationContext::instance().addDependency(*this);
   return value_;
}

template <typename T> void Property<T>::clearDependent()
{
   I_Property* dependent = qobject_cast<I_Property*>(sender());
   if(dependent)
   {
      PropertyEvaluationContext::instance().removeDependency(*dependent, *this);
   }
}

template<typename T> void Property<T>::evaluate()
{
   if(binding_)
   {
      PropertyEvaluationContext::instance().setContext(*this);
      value_ = binding_();
      PropertyEvaluationContext::instance().restoreContext();
      emit changed();
   }
}
