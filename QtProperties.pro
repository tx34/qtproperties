QT += testlib

CONFIG += c++11

SOURCES += PropertyEvaluationContext.cpp \
    TestProperty.cpp

HEADERS += \
    I_Property.h \
    Property.h \
    I_PropertyEvaluationContext.h \
    PropertyEvaluationContext.h
