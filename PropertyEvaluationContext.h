/*
Original work Copyright (C) 2013 Olivier Goffart <ogoffart@woboq.com>
http://woboq.com/blog/property-bindings-in-cpp.html
Modified work Copyright (c) 2014 James Brown

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once

#include <I_PropertyEvaluationContext.h>

class PropertyEvaluationContext : public I_PropertyEvaluationContext
{
public:
   PropertyEvaluationContext();

   virtual void setContext(const I_Property& property);
   virtual void restoreContext();
   virtual void addDependency(const I_Property& property);
   virtual void removeDependency(const I_Property& context, const I_Property& dependency);
   virtual bool isValidContext(const I_Property& property) const;

   static I_PropertyEvaluationContext& instance();

private:
   const I_Property* current_;
   const I_Property* previous_;
};
