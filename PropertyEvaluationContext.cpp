/*
Copyright (c) 2014 James Brown

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <PropertyEvaluationContext.h>
#include <QObject>
#include <I_Property.h>

PropertyEvaluationContext::PropertyEvaluationContext()
   : current_(nullptr)
   , previous_(nullptr)
{
}

void PropertyEvaluationContext::setContext(const I_Property& property)
{
   previous_ = current_;
   current_ = &property;
}

void PropertyEvaluationContext::restoreContext()
{
   current_ = previous_;
   previous_ = nullptr;
}

void PropertyEvaluationContext::addDependency(const I_Property& property)
{
   if(isValidContext(property))
   {
      QObject::connect(&property, &I_Property::changed,
         current_, &I_Property::evaluate, Qt::UniqueConnection);
      QObject::connect(current_, &I_Property::reset,
         &property, &I_Property::clearDependent, Qt::UniqueConnection);
   }
}

void PropertyEvaluationContext::removeDependency(const I_Property& context, const I_Property& dependency)
{
   QObject::disconnect(&dependency, &I_Property::changed, &context, &I_Property::evaluate);
   QObject::disconnect(&context, &I_Property::reset, &dependency, &I_Property::clearDependent);
}

bool PropertyEvaluationContext::isValidContext(const I_Property& property) const
{
   return current_ && current_ != &property;
}

I_PropertyEvaluationContext& PropertyEvaluationContext::instance()
{
   static PropertyEvaluationContext instance;
   return instance;
}
