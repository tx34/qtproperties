/*
Copyright (c) 2014 James Brown

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <Property.h>
#include <QtTest/QtTest>
#include <functional>

class TestProperty : public QObject
{
    Q_OBJECT

public:
   TestProperty()
      : value1_(0)
      , value2_(0)
      , value3_(0)
      , value4_(0)
   {
   }

private slots:
   void init()
   {
      value1_ = 1;
      value2_ = 2;
      value3_ = [&](){return value1_ + value2_;};
      value4_ = std::bind(&TestProperty::addProperties, this);
   }

   void valuePropertiesReturnCorrectValue()
   {
      int value1 = value1_;
      int value2 = value2_;
      QCOMPARE(value1, 1);
      QCOMPARE(value2, 2);
   }

   void dependentPropertiesReturnCorrectValue()
   {
      int value3 = value3_;
      QCOMPARE(value3, 3);
   }

   void updatesDependantPropertiesReturnValue()
   {
      value1_ = 2;
      int value3 = value3_;

      QCOMPARE(value3, 4);
   }

   void notifiesChangedWhenUpdatingDependentProperties()
   {
      QSignalSpy spy(&value3_, SIGNAL(changed()));

      value2_ = 3;

      QCOMPARE(spy.count(), 1);
   }

   void noLongerNotifiesChangedOnOldDependentsChangedWhenValueReset()
   {
      value3_ = 0;
      QSignalSpy spy(&value3_, SIGNAL(changed()));

      value1_ = 3;

      QCOMPARE(spy.count(), 0);
   }

   void calculatesMultipleLevelsOfDependents()
   {
      int value4 = value4_;

      QCOMPARE(value4, 6);
   }

   void canCopyByValueBeforeThereAreDependents()
   {
      std::vector<Property<int>> foo;
      foo.push_back(Property<int>(0));
   }

private:
   int addProperties()
   {
      return value1_ + value2_ + value3_;
   }

private:
    Property<int> value1_;
    Property<int> value2_;
    Property<int> value3_;
    Property<int> value4_;
};

QTEST_MAIN(TestProperty)
#include <TestProperty.moc>
